from django.conf.urls import url, include
from django.contrib import admin
from djangoblog import views

urlpatterns = [
    url(r'^$', views.HomePage.as_view(), name='index'),

    url(r'^login$', views.Login.as_view(), name='login'),
    url(r'^logout$', views.logout_view, name='logout'),
    url(r'^register$', views.Register.as_view(), name='register'),

    url(r'^postlist$', views.PostList.as_view(), name='post_list'),
    url(r'^postlistcategory/(?P<pk>[0-9]+)/postcontent$', views.PostContent.as_view(), name='post_content'),
    url(r'^postlistcategory/(?P<pk>[0-9]+)/$', views.PostListCategory.as_view(), name='post_list_category'),
    url(r'^profile/(?P<pk>[0-9]+)', views.ProfileView.as_view(), name='profile_content'),
    url(r'^seeprofile/(?P<pk>[0-9]+)', views.SeeProfile.as_view(), name='see_profile'),
    url(r'^editprofile/(?P<pk>[0-9]+)', views.EditProfile, name='edit_profile'),

    url(r'^new$', views.CreateCategory.as_view(), name='category_new'),
    url(r'^edit/(?P<pk>\d+)$', views.category_update, name='category_edit'),
    url(r'^delete/(?P<pk>\d+)$', views.category_delete, name='category_delete'),

    url(r'^newpost$', views.CreatePost.as_view(), name='post_new'),
    url(r'^postedit/(?P<pk>\d+)$', views.post_update, name='post_edit'),
    url(r'^postdelete/(?P<pk>\d+)$', views.post_delete, name='post_delete'),

    url(r'^commentedit/(?P<pk>\d+)$', views.comment_update, name='comment_edit'),
    url(r'^commentdelete/(?P<pk>\d+)$', views.comment_delete, name='comment_delete'),
    url(r'^commentlike/(?P<post_id>[0-9]+)/(?P<pk>\d+)$', views.comment_like, name='comment_like'),
    url(r'^commentdislike/(?P<post_id>[0-9]+)/(?P<pk>\d+)$', views.comment_dislike, name='comment_dislike'),

    url(r'^api/', include('djangorestframework.urls', namespace='djangorestframework')),

    url(r'^admin/', admin.site.urls),
]

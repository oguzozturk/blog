
from django.contrib import admin
from djangoblog.models import Category, Post, Comment


class BlogAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_display2 = ('post_title', 'content')
    search_fields = ['title']

admin.site.register(Category, BlogAdmin)
admin.site.register(Post)
admin.site.register(Comment)

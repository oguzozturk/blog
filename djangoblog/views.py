import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import Http404
from django.views import generic
from django.shortcuts import render, redirect, get_object_or_404
from djangoblog.forms import CategoryForm, PostForm, RegisterForm, LoginForm, CommentForm, VoteForm, ProfileEditForm
from djangoblog.models import Category, Post, Comment
from django.http import HttpResponseRedirect


class HomePage(generic.View):
    template_name = "index.html"
    context_object_name = "category_list"

    def get(self, request):
        try:
            category_list = Category.objects.all()
            post_list = Post.objects.order_by('-id')[:5]
            posts = Post.objects.all()

        except Category.DoesNotExist:
            raise Http404("Category does not exist")
        context = {'category_list': category_list, 'post_list': post_list}
        return render(request, 'index.html', context)


class Login(generic.View):
    model = User
    form_class = LoginForm()

    def get(self, request):
        login_form = LoginForm()
        return render(request, "login.html",
                      {'login_form': login_form})

    def post(self, request):
        login_form = LoginForm(request.POST)

        if login_form.is_valid():
            username = login_form.data.get('username')
            password = login_form.data.get('password')
            user = authenticate(username=username, password=password)

            if user:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    return render(request, 'login.html', {'login_form': login_form})

            else:
                return render(request, 'login.html', {'login_form': login_form})


def logout_view(request):
    logout(request)
    return redirect('login')


class Register(generic.View):
    model = User
    form_class = RegisterForm()

    def get(self, request):
        register_form = RegisterForm()
        return render(request, "register.html",
                      {'register_form': register_form})

    def post(self, request):
        register_form = RegisterForm(data=request.POST)

        if register_form.is_valid():
            user = User.objects.create(username=register_form.data.get('username'),
                                       email=register_form.data.get('email'),
                                       first_name=register_form.data.get('firstname'),
                                       last_name=register_form.data.get('lastname'))
            user.set_password(register_form.data.get('password'))
            user.save()
            return redirect('login')

        else:
            return render(request, 'register.html', {'register_form': register_form})


class PostList(generic.ListView):
    model = Post
    template_name = "post_list.html"


class PostListCategory(generic.DetailView, generic.View):
    model = Category
    template_name = "post_list_category.html"


class PostContent(generic.DetailView, generic.View):
    model = Post
    template_name = "post_content.html"

    def get(self, request, pk):
        comment_form = CommentForm()
        vote_form = VoteForm()
        post = Post.objects.get(pk=pk)

        return render(request, "post_content.html",
                      {'comment_form': comment_form,
                       'vote_form': vote_form,
                       'post': post, 'like': post.like, 'dislike': post.dislike})

    def post(self, request, pk):
        comment_form = CommentForm(data=request.POST)
        vote_form = VoteForm(data=request.POST)
        vote = vote_form.data.get('like')
        post = Post.objects.get(pk=pk)

        if vote_form.is_valid():
            if vote == 'Like':
                count = post.like
                count += 1
                post.like = count
                post.save()
            if vote == 'Dislike':
                count = post.dislike
                count += 1
                post.dislike = count
                post.save()

            return HttpResponseRedirect('/postlistcategory/%s/postcontent' % pk)

        else:
            if comment_form.is_valid():
                Comment.objects.create(post_id=pk,
                                       author_id=request.user.id,
                                       comment_text=comment_form.data.get('comment_text'))
                return HttpResponseRedirect('/postlistcategory/%s/postcontent' % pk)

            else:
                post = Post.objects.get(pk=pk)

                return render(request, "post_content.html",
                              {'comment_form': comment_form,
                               'vote_form': vote_form,
                               'post': post, 'like': post.like, 'dislike': post.dislike})


class ProfileView(generic.View):
    model = User
    template_name = "profile_content.html"

    def get(self, request, pk):
        post_list = Post.objects.filter(author_id=pk)
        user_profile = User.objects.get(pk=pk)
        return render(request, "profile_content.html", {'user_profile': user_profile, 'post_list': post_list})


class SeeProfile(generic.View):
    model = User
    template_name = "see_profile.html"

    def get(self, request, pk):
        post_list = Post.objects.filter(author_id=pk)
        user_profile = User.objects.get(pk=pk)
        return render(request, "see_profile.html", {'user_profile': user_profile, 'post_list': post_list})


def EditProfile(request, pk, template_name='profile_edit_form.html'):
    if not (request.user.is_authenticated()):
        return redirect('login')
    else:

        user = get_object_or_404(User, pk=pk)
        if user:
            profile_edit_form = ProfileEditForm(request.POST or None, instance=user)
            if profile_edit_form.is_valid():
                user.updated_at = datetime.datetime.now().replace(microsecond=0)
                user.save()
                profile_edit_form.save()
                return HttpResponseRedirect('/seeprofile/%s/' % user.id)
            return render(request, template_name, {'profile_edit_form': profile_edit_form})
        else:
            return redirect('index')


class CreateCategory(generic.View):
    model = Category
    template_name = "category_form.html"
    form_class = CategoryForm()

    def get(self, request):
        if not (request.user.is_authenticated() and request.user.is_superuser):
            return redirect('login')
        else:
            category_form = CategoryForm()
            return render(request, "category_form.html",
                          {'category_form': category_form})

    def post(self, request):
        if not (request.user.is_authenticated() and request.user.is_superuser):
            return redirect('login')
        else:
            category_form = CategoryForm(data=request.POST)

            if category_form.is_valid():
                Category.objects.create(title=category_form.data.get('title'))
                return HttpResponseRedirect('/')

            else:
                return render(request, 'category_form.html', {'category_form': category_form})


class CreatePost(generic.View):
    template_name = "post_form.html"
    model = Category
    form_class = PostForm()

    def get(self, request):
        if not (request.user.is_authenticated()):
            return redirect('login')
        else:
            post_form = PostForm()
            return render(request, "post_form.html", {'post_form': post_form})

    def post(self, request):
        post_form = PostForm(data=request.POST)

        if post_form.is_valid():
            if request.user.is_authenticated():
                Post.objects.create(author_id=request.user.id,
                                    category_id=post_form.data.get('category'),
                                    post_title=post_form.data.get('post_title'),
                                    content=post_form.data.get('content'))

            return HttpResponseRedirect('/')

        else:
            return render(request, 'post_form.html', {'post_form': post_form})


def category_update(request, pk, template_name='category_form.html'):
    if not (request.user.is_authenticated() and request.user.is_superuser):
        return redirect('login')
    else:
        category = get_object_or_404(Category, pk=pk)
        category_form = CategoryForm(request.POST or None, instance=category)
        if category_form.is_valid():
            category_form.save()
            return redirect('index')
        return render(request, template_name, {'category_form': category_form})


def category_delete(request, pk, template_name='category_confirm_delete.html'):
    if not (request.user.is_authenticated() and request.user.is_superuser):
        return redirect('login')
    else:
        category = get_object_or_404(Category, pk=pk)

        if request.method == 'POST':
            if Category.objects.filter(title='Unused'):
                cat = Category.objects.get(title='Unused')
                posts = Post.objects.filter(category_id=pk)

                for post in posts:
                    post.category = cat
                    post.save()
                if not (category.title == 'Unused'):
                    category.delete()

            else:
                Category.objects.create(title='Unused')
                cat = Category.objects.get(title='Unused')
                posts = Post.objects.filter(category_id=pk)

                for post in posts:
                    post.category = cat
                    post.save()
                if not (category.title == 'Unused'):
                    category.delete()
            return redirect('index')
        return render(request, template_name, {'object': category})


def post_update(request, pk, template_name='post_update.html'):
    if not (request.user.is_authenticated()):
        return redirect('login')
    else:

        post = get_object_or_404(Post, pk=pk)
        if post.author == request.user or request.user.is_superuser:
            post_form = PostForm(request.POST or None, instance=post)
            if post_form.is_valid():
                post.updated_at = datetime.datetime.now().replace(microsecond=0)
                post.save()
                post_form.save()
                return HttpResponseRedirect('/postlistcategory/%s/postcontent' % post.id)
            return render(request, template_name, {'post_form': post_form})
        else:
            return redirect('index')


def post_delete(request, pk, template_name='category_confirm_delete.html'):
    if not (request.user.is_authenticated()):
        return redirect('login')
    else:
        post = get_object_or_404(Post, pk=pk)
        id = post.id
        if post.author == request.user or request.user.is_superuser:
            if request.method == 'POST':
                post.delete()
                return redirect('index')
            return render(request, template_name, {'object': post})
        else:
            return redirect('index')


def comment_update(request, pk, template_name='template.html'):
    if not (request.user.is_authenticated()):
        return redirect('login')
    else:
        comment = get_object_or_404(Comment, pk=pk)
        comment_form = CommentForm(request.POST or None)
        if comment.author == request.user or request.user.is_superuser:
            if comment_form.is_valid():
                comment.comment_text = comment_form.data.get('comment_text')
                comment.updated_comment_at = datetime.datetime.now().replace(microsecond=0)
                comment.save()
                return HttpResponseRedirect('/postlistcategory/%s/postcontent' % comment.post.id)
            return render(request, template_name, {'comment_form': comment_form})
        else:
            return redirect('index')


def comment_delete(request, pk, template_name='category_confirm_delete.html'):
    if not (request.user.is_authenticated()):
        return redirect('login')
    else:
        comment = get_object_or_404(Comment, pk=pk)
        if comment.author == request.user or request.user.is_superuser:
            if request.method == 'POST':
                comment.delete()
                return HttpResponseRedirect('/postlistcategory/%s/postcontent' % comment.post.id)
            return render(request, template_name, {'object': comment})
        else:
            return redirect('index')


def comment_like(request, post_id, pk, template_name='post_content.html'):
    if not (request.user.is_authenticated()):
        return redirect('login')
    else:
        comment = Comment.objects.get(id=pk)
        if request.method == 'GET':
            comment.like_comment += 1
            comment.save()
            return HttpResponseRedirect('/postlistcategory/%s/postcontent' % post_id)
        return render(request, template_name)


def comment_dislike(request, post_id, pk, template_name='post_content.html'):
    if not (request.user.is_authenticated()):
        return redirect('login')
    else:
        comment = Comment.objects.get(id=pk)
        if request.method == 'GET':
            comment.dislike_comment += 1
            comment.save()
            return HttpResponseRedirect('/postlistcategory/%s/postcontent' % post_id)
        return render(request, template_name)

from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm

from djangoblog.models import Category, Post


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ['title']


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['category', 'post_title', 'content', ]


class RegisterForm(forms.Form):
    username = forms.CharField(label='UserName', max_length=50)
    email = forms.CharField(label='Email', max_length=50)
    firstname = forms.CharField(label='FirstName', max_length=50)
    lastname = forms.CharField(label='LastName', max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())


class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())


class CommentForm(forms.Form):
    comment_text = forms.CharField(max_length=300)


class ProfileEditForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class VoteForm(forms.Form):
    CHOICES = [('Like', 'Like'),
               ('Dislike', 'Dislike')]
    like = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())

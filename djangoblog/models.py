from audioop import reverse

from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('category_edit', kwargs={'pk': self.pk})


class Post(models.Model):
    category = models.ForeignKey(Category)
    author = models.ForeignKey(User)
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    post_title = models.TextField(max_length=100)
    content = models.TextField(max_length=5000)

    def __str__(self):
        return self.post_title

    def __unicode__(self):
        return self.post_title

    def get_absolute_url(self):
        return reverse('post_edit', kwargs={'pk': self.pk})


class Comment(models.Model):
    post = models.ForeignKey(Post)
    author = models.ForeignKey(User)
    comment_text = models.TextField(max_length=300)
    like_comment = models.IntegerField(default=0)
    dislike_comment = models.IntegerField(default=0)
    created_comment_at = models.DateTimeField(auto_now_add=True)
    updated_comment_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.comment_text

    def __unicode__(self):
        return self.comment_text

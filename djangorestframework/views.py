from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.views import APIView
from djangoblog.models import Post, Category, Comment
from djangorestframework.serializers import PostSerializer, CategorySerializer, CommentSerializer, ProfileSerializer, \
    AuthSerializer
from rest_framework import status


class CategoryViewset(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = CategorySerializer(data=request.data)
        if request.user.is_superuser:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                return Response({'detail': 'Data Error'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        if pk and request.user.is_superuser:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response({'detail': serializer.data}, status=status.HTTP_202_ACCEPTED)

        else:
            return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        category = Category.objects.get(id=kwargs.get('pk'))

        if not (request.user.is_authenticated() and request.user.is_superuser):
            return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)
        else:

            if Category.objects.filter(title='Unused'):
                cat = Category.objects.get(title='Unused')
                posts = Post.objects.filter(category_id=kwargs.get('pk'))

                for post in posts:
                    post.category = cat
                    post.save()
                if not (category.title == 'Unused'):
                    category.delete()
                    return Response({'detail': 'Deleted'}, status=status.HTTP_202_ACCEPTED)

            else:
                Category.objects.create(title='Unused')
                cat = Category.objects.get(title='Unused')
                posts = Post.objects.filter(category_id=kwargs.get('pk'))

                for post in posts:
                    post.category = cat
                    post.save()
                if not (category.title == 'Unused'):
                    category.delete()
                    return Response({'detail': 'Deleted'}, status=status.HTTP_202_ACCEPTED)


class PostViewset(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = PostSerializer(data=request.data)
        if request.user.is_authenticated():
            if serializer.is_valid():
                serializer.save()
                return Response({'detail': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Data Error'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Not Authenticated'}, status=status.HTTP_401_UNAUTHORIZED)

    def update(self, request, *args, **kwargs):
        post = Post.objects.get(id=kwargs.get('pk'))
        if post.author_id == request.user.id or request.user.is_superuser:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response({'detail': serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        post = Post.objects.get(id=kwargs.get('pk'))
        if post.author_id == request.user.id or request.user.is_superuser:
            post.delete()
            return Response({'detail': 'Deleted'}, status=status.HTTP_200_OK)
        return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)


class CommentViewset(viewsets.ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = CommentSerializer(data=request.data)
        if request.user.is_authenticated():
            if serializer.is_valid():
                serializer.save()
                return Response({'detail': serializer.data}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Data Error'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        comment = Comment.objects.get(id=kwargs.get('pk'))
        if comment.author_id == request.user.id or request.user.is_superuser:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response({'detail': serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        comment = Comment.objects.get(id=kwargs.get('pk'))
        if comment.author_id == request.user.id or request.user.is_superuser:
            comment.delete()
            return Response({'detail': 'Deleted'}, status=status.HTTP_200_OK)
        return Response({'detail': 'Permission Denied'}, status=status.HTTP_400_BAD_REQUEST)


class ProfileViewset(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer
    queryset = User.objects.all()


class PostLikeViewset(APIView):
    def get(self, request, pk):
        if pk:
            post = Post.objects.get(pk=pk)
            post.like += 1
            post.save()
            return Response({'detail': 'Liked'}, status=status.HTTP_200_OK)
        return Response({'detail': 'Error'}, status=status.HTTP_400_BAD_REQUEST)


class PostDislikeViewset(APIView):
    def get(self, request, pk):
        if pk:
            post = Post.objects.get(pk=pk)
            post.dislike += 1
            post.save()
            return Response({'detail': 'Disliked'}, status=status.HTTP_200_OK)
        return Response({'detail': 'Error'}, status=status.HTTP_400_BAD_REQUEST)


class CommentLikeViewset(APIView):
    def get(self, request, pk):
        if pk:
            comment = Comment.objects.get(pk=pk)
            comment.like_comment += 1
            comment.save()
            return Response({'detail': 'Liked'}, status=status.HTTP_200_OK)
        return Response({'detail': 'Error'}, status=status.HTTP_400_BAD_REQUEST)


class CommentDislikeViewset(APIView):
    def get(self, request, pk):
        if pk:
            comment = Comment.objects.get(pk=pk)
            comment.dislike_comment += 1
            comment.save()
            return Response({'detail': 'Disliked'}, status=status.HTTP_200_OK)
        return Response({'detail': 'Error'}, status=status.HTTP_400_BAD_REQUEST)


class AuthViewset(viewsets.ModelViewSet):
    serializer_class = AuthSerializer

    def get(self, request, format=None):
        return Response({'detail': "GET answer"})

    def post(self, request, format=None):
        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
        if "username" not in data or "password" not in data:
            return Response(
                'Wrong credentials',
                status=status.HTTP_401_UNAUTHORIZED
            )
        try:
            user = User.objects.get(username=data.get('username'))
        except:
            user = None
        if user is None:
            return Response(
                'No default user, please create one',
                status=status.HTTP_404_NOT_FOUND
            )

        token = Token.objects.get_or_create(user=user)

        return Response({'detail': 'POST answer',
                         'token': token[0].key,
                         'user': token[0].user_id,
                         'username': token[0].user.username,
                         'first_name': token[0].user.first_name,
                         'last_name': token[0].user.last_name,
                         'is_superuser': token[0].user.is_superuser,
                         'email': token[0].user.email})

    def destroy(self, request, format=None):
        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )

        if User.objects.get(username=str(data.get('username'))):
            user = User.objects.get(username=str(data.get('username')))
            token = Token.objects.get(user=user)
            token.delete()
            return Response({'detail': 'DESTROY answer'}, status=status.HTTP_200_OK)

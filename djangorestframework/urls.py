from rest_framework import routers
from django.conf.urls import url, include

from djangorestframework import views
from djangorestframework.views import CategoryViewset

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewset, base_name='categories')
router.register(r'posts', views.PostViewset, base_name='posts')
router.register(r'comments', views.CommentViewset, base_name='comments')
router.register(r'users', views.ProfileViewset, base_name='users')

urlpatterns = [
    url(r'^', include(router.urls)),

    url(r'^categories/create$', views.CategoryViewset.as_view({'get': 'create'}),
        name='category_create'),
    url(r'^categories/(?P<pk>\d+)/update$', views.CategoryViewset.as_view({'get': 'put'}),
        name='category_update'),
    url(r'^categories/(?P<pk>\d+)/delete$', views.CategoryViewset.as_view({'get': 'destroy'}),
        name='category_delete'),

    url(r'^posts/create$', views.PostViewset.as_view({'get': 'create'}),
        name='post_create'),
    url(r'^posts/(?P<pk>\d+)$/update', views.PostViewset.as_view({'get': 'put'}),
        name='post_update'),
    url(r'^posts/(?P<pk>\d+)$/delete', views.PostViewset.as_view({'get': 'destroy'}),
        name='post_delete'),

    url(r'^comments/create$', views.CommentViewset.as_view({'get': 'create'}),
        name='comment_create'),
    url(r'^comments/(?P<pk>\d+)/update$', views.CommentViewset.as_view({'get': 'put'}),
        name='template'),
    url(r'^comments/(?P<pk>\d+)/delete$', views.CommentViewset.as_view({'get': 'destroy'}),
        name='comment_delete'),

    url(r'^comments/(?P<pk>[0-9]+)/like$', views.CommentLikeViewset.as_view(),
        name='comments_like'),
    url(r'^comments/(?P<pk>[0-9]+)/dislike$', views.CommentDislikeViewset.as_view(),
        name='comments_dislike'),
    url(r'^posts/(?P<pk>[0-9]+)/like$', views.PostLikeViewset.as_view(),
        name='posts_like'),
    url(r'^posts/(?P<pk>[0-9]+)/dislike$', views.PostDislikeViewset.as_view(),
        name='posts_dislike'),

    url(r'^auth/', views.AuthViewset.as_view({'get': 'get', 'post': 'post', 'delete': 'destroy'}), name='auth_view'),
]
